export default function interpolateDictionaryValues(dictionary){
    if(Object.prototype.toString.call(dictionary) !== "[object Object]"){
        throw new Error("Please provide correct dictionary");
    }
    const pendingKeys = {};

    for(let key in dictionary){
        if(dictionary.hasOwnProperty(key)){
            dictionary[key] = resolve(dictionary, key, pendingKeys);
        }
    }

    return dictionary;
}

function resolve(dict, key, pendingKeys){
    let value = dict[key];

    if (value === undefined) {
        throw new Error("There is no such key '" + key + "' in the dictionary. Please check and try again.");
    }
    if(!isString(value)){
        throw new Error("Value of key '" + key + "' is not a string. Please check and try again.");
    }
    if(pendingKeys[key]){
        throw new Error("There is a reference cycle in the dictionary. Key '" + key + "' is required, but it's value is not set. Please check and try again.");
    }

    pendingKeys[key] = true;

    let newStr = dict[key].replace(/(\${([^}]+)})/g, function(i) {
        let k = i.replace(/\$/, '').replace(/{/, '').replace(/}/, '');

        return resolve(dict, k, pendingKeys);
    });

    pendingKeys[key] = false;

    return newStr;
}

function isString(s){
    return typeof s === "string" || s instanceof String;
}