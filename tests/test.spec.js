"use strict";
var expect = require("chai").expect;

import interpolateDictionaryValues from '../lib/index';

describe("pass correct values", function () {
    it('should return the same empty dictionary for empty dictionary', function(){
       let result = interpolateDictionaryValues({});
       expect(result).to.eql({});
    });

    it('should return the same dictionary in case if there is no key patterns in values', function () {
        let dictionary = {
            "key1": "value",
            "key2": "value"
        };
        let result = interpolateDictionaryValues(dictionary);

        expect(result).to.eql({
            "key1": "value",
            "key2": "value"
        });
    });

    it('should return updated dictionary when there is key single pattern in values', function(){
        let dictionary = {
          "key1": "value1",
          "key2": "${key1}value2"
        };

        let result = interpolateDictionaryValues(dictionary);

        expect(result).to.eql({
           "key1": "value1",
           "key2": "value1value2"
        });
    });

    it('should return updated dictionary when there are multiple key references in values', function(){
        let dictionary = {
            "key1": "value1",
            "key2": "value2",
            "key3": "${key1} some value ${key2}"
        };

        let result = interpolateDictionaryValues(dictionary);

        expect(result).to.eql({
            "key1": "value1",
            "key2": "value2",
            "key3": "value1 some value value2"
        });
    })

    it('should return updated dictionary when values references to keys which values are also have references', function(){
        let dictionary = {
            "key1": "value1",
            "key2": "value2",
            "key3": "${key1} some value ${key2}",
            "key4": "${key1} and ${key3}"
        };

        let result = interpolateDictionaryValues(dictionary);

        expect(result).to.eql({
            "key1": "value1",
            "key2": "value2",
            "key3": "value1 some value value2",
            "key4": "value1 and value1 some value value2"
        });
    });
});

describe("pass incorrect input", function(){
    it('should throw an error if there is no dictionary provided', function(){
        expect(interpolateDictionaryValues).to.throw();
    });

    it('should throw an error when some keys are missing and message should include missing key name', function(){
        let dictionary = {
            "key1": "value1",
            "key2": "${key3}value"
        }

        expect(function () {interpolateDictionaryValues(dictionary) }).to.throw(/key3/);
    });

    it('should throw an error when some values are not strings and message should include key name of that property', function(){
        let dictionary = {
            "key1": "value1",
            "key2": 12
        }

        expect(function () {interpolateDictionaryValues(dictionary) }).to.throw(/key2/);
    });

    it('should throw an error when some values are null and message should include key name of that property', function(){
        let dictionary = {
            "key1": "value1",
            "key2": null
        }

        expect(function () {interpolateDictionaryValues(dictionary) }).to.throw(/key2/);
    });

    it('should throw an error when there is a cycle reference and message should include key name', function(){
        let dictionary = {
            "key1": "value1",
            "key2": "${key3} value",
            "key3": "${key1} ${key2}"
        }

        expect(function () {interpolateDictionaryValues(dictionary) }).to.throw(/key2/);
    });
    
});

