Objective: Write a short program that resolves variable values in an input Map.

3-rd party libraries that are used:

- Mocha and Chai for unit tests
- Babel, to convert code with new ECMAScript features into a backwards compatible version of JavaScript

I assume that in case if value has a reference to a missing key it shouldn't be resolved as plain text and an error should be thrown.  
I also assume that null values should be recongnized as a wrong input and not as an empty string.